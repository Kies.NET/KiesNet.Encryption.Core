﻿using System.Security;

namespace KiesNet.Encryption.Core
{
    public static partial class StringExtensions
    {
        /// <summary>
        /// Converts a normal string into a secure string
        /// </summary>
        public static SecureString ToSecureString(this string stringToConvert)
        {
            if (string.IsNullOrEmpty(stringToConvert))
                return null;

            SecureString secureString = new SecureString();
            
            foreach (char character in stringToConvert)
                secureString.AppendChar(character);

            return secureString;
        }
    }
}
