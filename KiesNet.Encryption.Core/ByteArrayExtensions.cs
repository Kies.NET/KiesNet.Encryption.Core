﻿using System.Text;

namespace KiesNet.Encryption.Core
{
    internal static class ByteArrayExtensions
    {
        /// <summary>
        /// Converts a byte array into a string
        /// </summary>
        public static string ConvertToString(this byte[] bytes)
        {
            if (bytes == null)
                return null;

            if (bytes.Length == 0)
                return string.Empty;

            StringBuilder builder = new StringBuilder();

            foreach (byte currentByte in bytes)
                builder.Append(currentByte.ToString("x2"));

            return builder.ToString();
        }
    }
}
