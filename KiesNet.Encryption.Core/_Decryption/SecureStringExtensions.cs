﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace KiesNet.Encryption.Core
{
    public static class SecureStringExtensions
    {
        /// <summary>
        /// Convert a securestring into a normal string
        /// </summary>
        public static string ToInsecureString(this SecureString secureString)
        {
            IntPtr intPtr = IntPtr.Zero;

            try
            {
                intPtr = Marshal.SecureStringToGlobalAllocUnicode(secureString);
                return Marshal.PtrToStringUni(intPtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(intPtr);
            }
        }
    }
}
