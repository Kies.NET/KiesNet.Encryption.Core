﻿
namespace KiesNet.Encryption.Core._Hashes._BCrypt
{
    public class BCrypt
    {
        public static string GenerateSalt(int workFactor = 10)
        {
            return global::BCrypt.Net.BCrypt.GenerateSalt(workFactor);
        }
    }
}
