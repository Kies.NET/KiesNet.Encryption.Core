﻿
namespace KiesNet.Encryption.Core
{
    public static partial class StringExtensions
    {
        public static string ToBCrypt(this string stringToHash, int workFactor = 10)
        {
            return BCrypt.Net.BCrypt.HashPassword(stringToHash, workFactor);
        }

        public static string ToBCrypt(this string stringToHash, string salt)
        {
            return BCrypt.Net.BCrypt.HashPassword(stringToHash, salt);
        }

        public static bool BCryptVerify(this string stringToCheck, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(stringToCheck, hash);
        }
    }
}
