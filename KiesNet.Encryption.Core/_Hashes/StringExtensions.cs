﻿using System.Security.Cryptography;
using System.Text;

namespace KiesNet.Encryption.Core
{
    public static partial class StringExtensions
    {
        /// <summary>
        /// Hashes a string with md5
        /// </summary>
        public static string ToMd5(this string stringToHash)
        {
            using (MD5 md5 = new MD5CryptoServiceProvider())
                return md5.ComputeHash(Encoding.Default.GetBytes(stringToHash)).ConvertToString();
        }

        /// <summary>
        /// Hashes a string with sha-1
        /// </summary>
        public static string ToSha1(this string stringToHash)
        {
            using (SHA1Managed sha1Managed = new SHA1Managed())
                return sha1Managed.ComputeHash(Encoding.UTF8.GetBytes(stringToHash)).ConvertToString();
        }

        /// <summary>
        /// Hashes a string with sha-256
        /// </summary>
        public static string ToSha256(this string stringToHash)
        {
            using (SHA256 sha256 = new SHA256CryptoServiceProvider())
                return sha256.ComputeHash(Encoding.UTF8.GetBytes(stringToHash)).ConvertToString();
        }

        /// <summary>
        /// Hashes a string with sha-512
        /// </summary>
        public static string ToSha512(this string stringToHash)
        {
            using (SHA512 sha512 = new SHA512CryptoServiceProvider())
                return sha512.ComputeHash(Encoding.UTF8.GetBytes(stringToHash)).ConvertToString();
        }

    }
}
